#iup-sys

A bindgen-generated static rust binding to the IUP 3.21 library.

Note that the included static libs are for windows and the mingw compiler. If you want to use the msvc version of rust, or linux, you'll have to replace the staticlib/ dir with the appropriate files from the IUP distribution.

Since this repo is largely just IUP, it carries the same license as that project. It is not endorsed by Tecgraf/PUC-Rio in any way

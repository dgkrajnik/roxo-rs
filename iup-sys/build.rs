fn main() {
    let mut libdir = std::env::current_dir().unwrap();
    libdir.push("staticlib");
    println!("cargo:rustc-link-search=native={}", libdir.to_str().unwrap());
    println!("cargo:rustc-link-lib=static=iup");
    #[cfg(windows)]
    {
        println!("cargo:rustc-link-lib=gdi32");
        println!("cargo:rustc-link-lib=comctl32");
        println!("cargo:rustc-link-lib=comdlg32");
        println!("cargo:rustc-link-lib=uuid");
    }
}

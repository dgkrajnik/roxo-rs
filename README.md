# Roxo

Roxo is a dmenu-inspired graphical file search/launcher program.

Roxo has no plugins, does not remain open in the background, and doesn't have any hotkeys; it starts fast, finds fast, and gets out of your way.

## Getting Roxo

Head to the [tags page](/../tags) to grab a fresh version of Roxo. Simply unzip with [7-zip](http://www.7-zip.org/download.html) and you're good to go.  
If you want to compile Roxo yourself, you'll need a nightly Windows distribution of rust, with the GNU ABI.

## Usage

To use Roxo, simply launch it.

Roxo will start immediately when launched, and you can start typing straight away. Roxo will match your query to files it discovers, ranked by closeness.

Navigate to different entries with up/down arrows, and press Enter to launch the highlighted file. Pressing Ctrl+Enter will open the location of the highlighted file.
Pressing Shift+Enter will attempt to open the highlighted file with Admin privileges, if possible.

Roxo can be started with admin privileges and will automatically run programs as the currently logged-on user instead.

Roxo will automatically close if it loses focus, if you launch a program, or if you press Escape.

## Configuration

If you want to change a Roxo setting, modify the included `config.yaml`. Details abound within.  
This file must be named `config.yaml` and be placed next to the roxo-rs executable.

In this file, you can also add additional paths to search in and extensions to search for.  
Note that Roxo will attempt to perform the system-default action for starting a file, as if you double clicked it in the file browser.

## Hotkeys

Roxo's target OS is Windows, which does not have a built-in way to assign a program to launch on a hotkey, so you'll have to set up a launcher.

For this, [AutoHotKey](https://autohotkey.com/download/) is a suggested solution. See below for an AutoHotKey 1.1.* script that will launch Roxo on a CTRL+SPACE keycombo.

```
^Space::Run, "<PATH TO ROXO-RS.EXE>"
```

A more complex example that replaces the solo functionality of the windows key, while retaining the ability to use windows key shortcuts:

```
#KeyHistory

canRun = 1

~LWin & ^::
~RWin & ^::
    canRun := 0
Return

~LWin & !::
~RWin & !::
    canRun := 0
Return

~LWin UP::
~RWin UP::
    if(A_PriorKey = "LWin")
    {
        if(canRun = 1)
        {
            Run, <Path To Roxo exe>
        }
    }
    else if(A_PriorKey = "RWin")
    {
        if(canRun = 1)
        {
            Run, <Path To Roxo exe>
        }
    }
    canRun := 1
Return
```

To use AutoHotKey to launch Roxo, first install AHK, then copy the above into a file with a `.ahk` extension, edit it appropriately, and run it. You may set this file to run on startup to always have access to Roxo.

## Context Menus

Roxo has support for being given a starting directory on the command line and entering a special exhaustive search mode. The command is of the form

```
<PATH TO ROXO-RS.EXE> --start_dir "<STARTING DIR>"
```

In this exhaustive search mode, Roxo will find all files in the heirarchy starting at this directory, with no blacklisting, filetype filtering, or depth limits.

If you're on windows, you can add this mode as a context menu entry with a .reg file in the following form

```
Windows Registry Editor Version 5.00

[HKEY_CLASSES_ROOT\Directory\Background\shell\Roxo Here\command]
@="\"C:\\Users\\You\\PathToRoxo\\roxo-rs.exe\" --start_dir \"%V\""

[HKEY_CURRENT_USER\Software\Classes\directory\Background\shell\Roxo Here\command]
@="\"C:\\Users\\You\\PathToRoxo\\roxo-rs.exe\" --start_dir \"%V\""

[HKEY_CLASSES_ROOT\Directory\shell\Roxo Here\command]
@="\"C:\\Users\\You\\PathToRoxo\\roxo-rs.exe\" --start_dir \"%V\""

[HKEY_CURRENT_USER\Software\Classes\directory\shell\Roxo Here\command]
@="\"C:\\Users\\You\\PathToRoxo\\roxo-rs.exe\" --start_dir \"%V\""
```

## Matching Details

Roxo tries to find matches in an intuitive way; the easiest way to figure it out is to try it, but if you're interested an explanation follows.

> **For the nerds**  
> Roxo will prefer filenames that contain the query as a substring that is closest to a longest prefix. If there aren't enough of these, it will also add filenames containing the query as a subsequence, preferring those of minimal insert distance.

Roxo will first attempt to find a `filename` that contains your `query` wholly. For example, if we had a filename `banana`, Roxo would consider this a match for the query `ban`, as well as the query `nan`, and `ana`.  
Roxo then ranks `filename`s by the `query`'s distance from the start -- `ban` is a better match to `banana` than `kanban`. Roxo also ranks based upon how close the `query`'s length is to the `filename` -- query `bana` is a better match to `banana` than query `ban`.

If Roxo cannot fill out all the results with this type of match, it will attempt to find *subsequence* matches -- a `filename` that contains the letters in your `query` in order. For example, if we had a filename `volcano`, Roxo would consider this a match for the query `olan` (v **ol** c **an** o).  
Roxo then ranks `filenames` by the number of steps to each `query` letter -- query `olan` is a better match to `volcano` (**v** ol **c** ano) than `molecular node` (**m** ol **ecul** a **r** node). Roxo also ranks based upon how close the `query`'s length is to the `filename` -- query `olan` is a better match to `tolcan` than `volcano`.

If Roxo still cannot fill out all the results with this type of match, it will break your query into words and treat each word as a sub-query. Roxo ranks these subqueries as above, and will also attempt to match against each component of each file's path. If any subquery cannot be matched against the file name or path, the entire query will be considered a mismatch.

## Known Issues

   * Roxo can't properly launch elevated .bat files due to Windows issues.

   * Roxo will display out-of-date entries from its file cache until the filesystem searchers have fully crawled their trees.

   * Roxo will crash unceremoniously if you try to boot an entry that doesn't exist (Such as one from an outdated file cache).

   * Roxo will crash *hard* if the cache.dat has somehow become corrupted. Make sure to delete it if you have issues!

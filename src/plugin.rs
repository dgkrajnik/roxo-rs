#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub struct Entry {
    pub displayname: String, // The name that the frontend will display for this entry.
    pub command: Option<String>, // The command that the frontend will execute for this entry.
    pub parameters: Option<String> // The parameters that will be passed to the given command.
}

pub trait Plugin {
    /// Order the plugin to perform an internal update to any structures it
    /// keeps. This function may only block as long as `timeout` milliseconds.
    ///
    /// # Return
    ///
    /// If this function returns `false`, the frontend should not at-
    /// tempt to re-query the plugin unless its search parameters change.
    /// `true` indicates that the internal state of the plugin has
    /// changed and so the same query may now return different results.
    fn update(&mut self, timeout: u64) -> bool;

    /// Make a request to the plugin, with the given query, and tell it to only
    /// return up to `max_results`. Note that this parameter is only a hint, and
    /// the frontend should truncate any additional results sent.
    fn search(&self, query: &str, max_results: usize) -> Vec<Entry>;
}

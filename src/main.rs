#![feature(asm)]

#[cfg(windows)]
extern crate winapi;
#[cfg(feature="timing")]
extern crate time;
#[macro_use]
extern crate serde_derive;
extern crate serde_yaml;
extern crate iup_sys;
#[macro_use]
extern crate iup;
extern crate walkdir;
extern crate ordered_float;
extern crate shellexpand;
extern crate shell_escape;
extern crate regex;
#[macro_use]
mod timing_macros;
mod filesystem_cache;
mod plugin;
mod cpl_consts;

use plugin::{Entry, Plugin};
use filesystem_cache::{FilesystemQuerier, Directory};
use std::env;
use iup::{IupWidget, IupGenericWidget, IupContainer, RediscoveredCast};
use shell_escape::windows::escape;
use std::borrow::Cow;

use winapi::shared::windef::{HWND};
use winapi::shared::minwindef::{BOOL, BYTE, DWORD, LPDWORD, HINSTANCE, PBOOL, LPVOID};
use winapi::shared::winerror::{ERROR_FILE_NOT_FOUND, ERROR_PATH_NOT_FOUND};
use winapi::um::minwinbase::{LPSECURITY_ATTRIBUTES, STILL_ACTIVE};
use winapi::um::winnt::{PSID, HANDLE, PHANDLE, MAXIMUM_ALLOWED,
                        LUID_AND_ATTRIBUTES, TOKEN_PRIVILEGES, PTOKEN_PRIVILEGES,
                        TOKEN_ADJUST_PRIVILEGES, TOKEN_DUPLICATE, TOKEN_READ,
                        SID_IDENTIFIER_AUTHORITY, PSID_IDENTIFIER_AUTHORITY, SE_PRIVILEGE_ENABLED,
                        PROCESS_QUERY_LIMITED_INFORMATION, LPCWSTR, LPWSTR};
use winapi::um::processthreadsapi::{STARTUPINFOW, PROCESS_INFORMATION, LPPROCESS_INFORMATION, LPSTARTUPINFOW,
                                    OpenProcess, OpenProcessToken,
                                    GetStartupInfoW, GetCurrentProcess, GetExitCodeProcess};
use winapi::um::shellapi::{ShellExecuteW};
use winapi::um::objbase::{COINIT_APARTMENTTHREADED, COINIT_DISABLE_OLE1DDE};
use winapi::um::winuser::{GetWindowThreadProcessId, GetShellWindow, GetForegroundWindow};
use winapi::um::winbase::{LookupPrivilegeValueW};
use winapi::um::errhandlingapi::{GetLastError};
use winapi::um::combaseapi::{CoInitializeEx};

use regex::RegexSet;

#[cfg(feature="timing")]
use std::cell::RefCell;

#[cfg(feature="timing")]
thread_local!(static BIGTIMER: RefCell<time::SteadyTime> = RefCell::new(time::SteadyTime::now()));

// Maximum number of milliseconds to let a cache update tick run before
// stopping it. Lets us control one of the major sources of blocking.
const UPDATE_TIMEOUT: u64 = 20;

// While this does reduce the ease of making a new native plugin a bit, we do
// get to avoid trait objects, which do incur a measurable memory use overhead
// and a bit of a performance overhead.
enum PluginTypes {
    Filesearch(FilesystemQuerier),
}

macro_rules! implement_plugin_types {
    ( $( $type:path ),* ) => {
        impl Plugin for PluginTypes {
            fn update(&mut self, timeout: u64) -> bool {
                match *self {
                    $( $type(ref mut fs) => fs.update(timeout), )*
                }
            }

            fn search(&self, query: &str, max_results: usize) -> Vec<Entry> {
                match *self {
                    $( $type(ref fs) => fs.search(query, max_results), )*
                }
            }
        }
    };
}

// Provide all of the paths for each plugin type you have.
implement_plugin_types!(PluginTypes::Filesearch);

// While it would be possible to implement arbitrary safe per-widget call-
// backs in the iup-rs layer, this would still not provide us context to the
// roxo-specific UI context without code roughly equivalent to this anyway.
macro_rules! implement_callback {
    ( $internal_function:ident, $callback_name:ident ) => {
        unsafe extern "C" fn $callback_name(ih: *mut iup_sys::Ihandle) -> i32 {
            // Every day we stray further from the light of the lord.
            let iw = IupGenericWidget::from_handle(ih);
            let ctx = iw.get_raw_attr("ROXO_CONTEXT");
            if ctx.is_null() {
                iup_sys::IUP_CLOSE
            } else {
                let ref mut this = *(ctx as *mut _Ui);
                this.$internal_function(ih)
            }
        }
    }
}

implement_callback!(cache_tick, cache_callback);
implement_callback!(up_arrow, up_callback);
implement_callback!(down_arrow, down_callback);
implement_callback!(enter_key, enter_callback);
implement_callback!(enter_shift_key, enter_shift_callback);
implement_callback!(enter_ctrl_key, enter_ctrl_callback);

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct ConfigTop {
    placement: ConfigPlacement,
    input: ConfigInput,
    results: ConfigResults,
    search: ConfigSearch,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct ConfigPlacement {
    y_offset: i32,
    x_offset: i32,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct ConfigInput {
    width: usize,
    padding: i32,
    background: String,
    foreground: String,
    selection_background: String,
    selection_foreground: String,
    font_family: String,
    font_size: i32,
    font_extras: String,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct ConfigResults {
    padding: i32,
    background: String,
    foreground: String,
    selected_background: String,
    selected_foreground: String,
    font_family: String,
    font_size: i32,
    details_font_size: i32,
    font_extras: String,
    selected_font_family: String,
    selected_font_size: i32,
    selected_details_font_size: i32,
    selected_font_extras: String,
    line_spacing: i32,
    max_displayed: usize,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct ConfigDirectory {
    directory: String,
    #[serde(default)]
    depth: Option<usize>,
    #[serde(default)]
    filetypes: Option<Vec<String>>,
    #[serde(default)]
    blacklist: Option<Vec<String>>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct ConfigSearch {
    directories: Vec<ConfigDirectory>,
    filetypes: Vec<String>,
    blacklist: Vec<String>,
    folder_mode_blacklist: Vec<String>,
    depth: usize,
    pollrate: usize,
    use_path: bool,
    path_priority: i64,
    shallow_path: bool,
    settings_priority: i64,
    subquery_penalty: i64,
    subquery_path_penalty: i64,
    subquery_accum_penalty: i64,
    filesystem_threads: u32,
    throttle_regularity: u64,
    throttle_duration: u64,
    input_wait: u64
}

// We track the bit of the result buffer that plugins pushed into to facilitate
// optimisation and persistence between updates
struct PluginHolder {
    plugin: PluginTypes,
    last_results: Vec<Entry>,
    result_dirty: bool,
}

// A 'holder' struct that proxies and holds the box for the true Ui.
// Boxing the `Ui` is a preventative measure; we use the raw pointer
// of the Ui object in the C layer and if it ever moves, God is not in
// his heaven. We box the Ui and hope the Box keeps things together.
struct Ui {
    ui: Box<_Ui>,
}

impl Ui {
    fn setup(paths: Vec<Directory>, config: ConfigTop, single_dir_mode: bool) -> Ui {
        Ui { ui: Box::new(_Ui::setup(paths, config, single_dir_mode)) }
    }

    fn launch(&mut self) {
        self.ui.launch();
    }
}

struct _Ui {
    queriers: Vec<PluginHolder>,
    result_buffer: Vec<Entry>,
    result_dirty: bool,
    selected_result: usize,
    last_query: String,
    inputbox: iup::IupText,
    resboxes: Vec<iup::IupBbox>,
    resbox_container: iup::IupBbox,
    window: iup::IupDialog,
    paths: Vec<Directory>,
    first_update: u64,
    config: ConfigTop,
    single_dir_mode: bool,

    result_font: String,
    result_detail_font: String,
    selected_result_font: String,
    selected_result_detail_font: String,
}

impl _Ui {
    fn setup(paths: Vec<Directory>, config: ConfigTop, single_dir_mode: bool) -> _Ui {
        timer_start!(setup_format);
        let compiled_input_font = format!("{0}, {1} {2}",
                                          config.input.font_family,
                                          config.input.font_extras,
                                          config.input.font_size);
        let compiled_result_font = format!("{0}, {1} {2}",
                                           config.results.font_family,
                                           config.results.font_extras,
                                           config.results.font_size);
        let compiled_result_detail_font = format!("{0}, {1} {2}",
                                                  config.results.font_family,
                                                  config.results.font_extras,
                                                  config.results.details_font_size);
        let compiled_selected_result_font = format!("{0}, {1} {2}",
                                                    config.results.selected_font_family,
                                                    config.results.selected_font_extras,
                                                    config.results.selected_font_size);
        let compiled_selected_result_detail_font = format!("{0}, {1} {2}",
                                                           config.results.selected_font_family,
                                                           config.results.selected_font_extras,
                                                           config.results.selected_details_font_size);
        let mut resboxes = Vec::new();
        let attribs = &format!("TITLE=\"Roxo\", FONT=\"{0}\",\
                       BORDER=NO, RESIZE=YES, MAXBOX=NO,\
                       BGCOLOR=\"{1}\", FGCOLOR=\"{2}\",\
                       CANFOCUS=YES, ACTIVE=YES, MINBOX=NO, MENUBOX=NO,\
                       SIMULATEMODAL=YES, EXPAND=VERTICAL,\
                       CUSTOMFRAME=YES, CUSTOMFRAMECAPTION=0, HIDETITLEBAR=YES,\
                       SAVEUNDER=NO",
                               compiled_input_font,
                               config.input.background,
                               config.input.foreground);
        let vattribs = "EXPAND=VERTICAL";
        // During testing with a default configuration, at 2730 characters
        // entered, the input box would go blank. Hence the choice here for
        // max character count NC=2729.
        let tattribs = &format!("BORDER=NO, BGCOLOR=\"{0}\",\
                        FGCOLOR=\"{1}\", NC=2729,\
                        SIZE={2}x, CANFOCUS=YES, FORMATTING=YES",
                                config.input.background,
                                config.input.foreground,
                                config.input.width * 4);
        let tsattribs = &format!("NMARGIN={0}x{0}", config.input.padding);
        let rbattribs = &format!("BORDER=NO, FGCOLOR=\"{0}\",\
                         FONT=\"{1}\",\
                         TITLE=\"YOU SHOULDN'T SEE THIS.\"",
                                 config.results.foreground,
                                 compiled_result_font);
        let rsattribs = &format!("BORDER=NO, FGCOLOR=\"{0}\",\
                         FONT=\"{1}\",\
                         TITLE=\"YOU SHOULDN'T SEE THIS.\",\
                         ALIGNMENT=ALEFT:ABOTTOM",
                                 config.results.foreground,
                                 compiled_result_detail_font);
        let rhattribs = &format!("ALIGNMENT=ACENTER, NGAP=8, NMARGIN={0}x{1}",
                                 config.results.padding,
                                 (config.results.line_spacing as f64 / 2.0) as i32);
        let rbbattribs = &format!("BGCOLOR=\"{0}\", EXPAND=YES, SIZE={1}x,\
                          VISIBLE=NO, FLOATING=IGNORE",
                                  config.results.background,
                                  config.input.width * 4);
        let rvbattribs = &format!("NMARGIN=0x{0}, VISIBLE=NO, FLOATING=IGNORE",
                                  (config.results.line_spacing as f64 / 2.0) as i32);
        let rvbbbattribs = &format!("BGCOLOR=\"{0}\", VISIBLE=NO, FLOATING=IGNORE",
                                    config.results.background);
        timer_end!(setup_format, 1);

        iup::set_global_attribute("UTF8MODE", "YES");

        timer_start!(setup_layout);
        for _ in 0..config.results.max_displayed {
            let resbox = with_attribs_and_children!(iup::IupBbox, rbbattribs, [
                                with_attribs_and_children!(iup::IupHbox, rhattribs, [
                                    with_attribs!(iup::IupLabel, rbattribs), with_attribs!(iup::IupLabel, rsattribs)
                                ])
                            ]);
            resboxes.push(resbox);
        }

        let mut resbox_container = with_attribs_and_children!(iup::IupBbox,
                                                              rvbbbattribs,
                                                              [{
                                                                   let mut resvbox = with_attribs!(iup::IupVbox, rvbattribs);
                                                                   resvbox.add_children(&mut resboxes);
                                                                   resvbox
                                                               }]);

        let mut inputbox = with_attribs!(iup::IupText, tattribs);

        {
        let format = with_attribs!(iup::IupUser, &format!("FGCOLOR=\"{0}\", BGCOLOR=\"{1}\"",
                                             config.input.foreground,
                                             config.input.background));
        unsafe {
            inputbox.set_raw_attr("ADDFORMATTAG_HANDLE", format.get_handle() as *const std::os::raw::c_char);
        }
        }

        let mut window = with_attribs_and_children!(iup::IupDialog, attribs, [
                             with_attribs_and_children!(iup::IupVbox, vattribs, [
                                 with_attribs_and_children!(iup::IupHbox, tsattribs, [inputbox]),
                                 resbox_container
                             ])
                         ]);
        timer_end!(setup_layout, 1);

        inputbox.set_callback("KILLFOCUS_CB", Some(loopout));
        inputbox.set_callback("K_ESC", Some(loopout));

        timer_start!(setup_show);
        let (inputw, inputh) = inputbox.get_intint_attribute("SIZE");

        // There's an issue where, depending on the specifics of timings, the y-
        // centering might apply to the maximum height of the dialog, instead of
        // just the visible portion (the input box), which places the input box
        // at an overly high vertical position. We rectify this by forcing the
        // size of the dialog to be the size of the input box during positioning.
        window.set_str_attribute("SIZE", &format!("{}x{}", inputw, inputh));

        window.move_xy(iup::XPos::Center, iup::YPos::Center);
        window.offset_xy(config.placement.x_offset, config.placement.y_offset);

        window.set_str_attribute("SIZE", &format!("{}x", config.input.width * 4));
        timer_end!(setup_show, 1);

        _Ui {
            queriers: Vec::new(),
            result_buffer: Vec::with_capacity(config.results.max_displayed),
            result_dirty: true,
            selected_result: 0,
            last_query: String::new(),
            inputbox: inputbox,
            resboxes: resboxes,
            resbox_container: resbox_container,
            window: window,
            paths: paths,
            first_update: config.search.input_wait,
            config: config,
            single_dir_mode: single_dir_mode,


            result_font: compiled_result_font,
            result_detail_font: compiled_result_detail_font,
            selected_result_font: compiled_selected_result_font,
            selected_result_detail_font: compiled_selected_result_detail_font,
        }
    }

    fn launch(&mut self) {
        let mut tick_timer = iup::IupTimer::new();
        tick_timer.set_callback("ACTION_CB", Some(cache_callback));

        tick_timer.set_delay(self.config.search.pollrate as i32);
        // HERE BE DRAGONS
        // Since Iup callbacks don't take user params, we have to hack
        // in access to `self`. To this end, we stuff a pointer to `self`
        // into the timer object and retrieve it in a callback. Yes, this
        // breaks aliasing and mutability rules. Yes, it's bad practice.
        // Yes, it works.
        unsafe {
            tick_timer.set_raw_attr("ROXO_CONTEXT", self as *mut _Ui as *const i8);
        }
        tick_timer.start();

        unsafe {
            let selfpointer = self as *mut _Ui as *const i8;
            self.inputbox.set_raw_attr("ROXO_CONTEXT", selfpointer);
        }

        self.inputbox.set_callback("K_UP", Some(up_callback));
        self.inputbox.set_callback("K_DOWN", Some(down_callback));
        self.inputbox.set_callback("K_CR", Some(enter_callback));
        self.inputbox.set_callback("K_sCR", Some(enter_shift_callback));
        self.inputbox.set_callback("K_cCR", Some(enter_ctrl_callback));

        unsafe {
            #[cfg(feature="timing")]
            {
                BIGTIMER.with(|f| {
                    let end_time = time::SteadyTime::now();
                    println!("Entering main loop after {}ms", (end_time-*f.borrow()).num_milliseconds());
                });
            }
            iup_sys::IupMainLoop();
        }

        // This is where we'll appear after the main loop, so we can do
        // all of our cleanup nonsense here.
        unsafe {
            iup_sys::IupDestroy(self.window.get_handle());
            iup_sys::IupClose();
        }
    }

    fn up_arrow(&mut self, _: *mut iup_sys::Ihandle) -> i32 {
        if self.selected_result > 0 {
            self.selected_result -= 1;
            self.result_dirty = true;
        }
        self.clip_selected_result();

        iup_sys::IUP_IGNORE
    }

    fn down_arrow(&mut self, _: *mut iup_sys::Ihandle) -> i32 {
        if self.result_buffer.len() > 0 && self.selected_result < self.result_buffer.len() - 1 {
            self.selected_result += 1;
            self.result_dirty = true;
        }
        self.clip_selected_result();

        iup_sys::IUP_IGNORE
    }

    fn enter_key(&mut self, _: *mut iup_sys::Ihandle) -> i32 {
        if self.selected_result < self.result_buffer.len() {
            if let Some(ref command) = self.result_buffer[self.selected_result].command {
                let params = match self.result_buffer[self.selected_result].parameters {
                    Some(ref x) => x,
                    None => ""
                };
                let commandfolder = std::path::Path::new(command).parent().unwrap_or(std::path::Path::new(""));
                if !deescalated_launch_file(command, params, commandfolder.to_str().unwrap()) {
                    return iup_sys::IUP_IGNORE;
                }
            }
        }
        loopout(std::ptr::null_mut());

        0
    }

    fn enter_shift_key(&mut self, _: *mut iup_sys::Ihandle) -> i32 {
        if self.selected_result < self.result_buffer.len() {
            if let Some(ref command) = self.result_buffer[self.selected_result].command {
                let params = match self.result_buffer[self.selected_result].parameters {
                    Some(ref x) => x,
                    None => ""
                };
                let commandfolder = std::path::Path::new(command).parent().unwrap_or(std::path::Path::new(""));
                if !escalated_launch_file(command, params, commandfolder.to_str().unwrap()) {
                    return iup_sys::IUP_IGNORE;
                }
            }
        }
        loopout(std::ptr::null_mut());

        0
    }

    fn enter_ctrl_key(&mut self, _: *mut iup_sys::Ihandle) -> i32 {
        if self.selected_result < self.result_buffer.len() {
            if let Some(ref command) = self.result_buffer[self.selected_result].command {
                let commandfolder = std::path::Path::new(command).parent().unwrap_or(std::path::Path::new(""));
                let commandfile = std::path::Path::new(command).file_name().unwrap_or(std::ffi::OsStr::new(""));

                if !launch_file("explorer.exe", &format!("/select,\"{}\"",  commandfile.to_str().unwrap()), commandfolder.to_str().unwrap(), false) {
                    return iup_sys::IUP_IGNORE;
                }
            }
        }
        loopout(std::ptr::null_mut());

        0
    }

    fn cache_tick(&mut self, _: *mut iup_sys::Ihandle) -> i32 {
        timer_start!(timer_tick);
        if !self.check_self_focused() {
            return iup_sys::IUP_CLOSE;
        }
        timer_start!(fresh_results);
        self.get_fresh_results();
        timer_end!(fresh_results, 1);
        timer_start!(selected_result);
        self.clip_selected_result();
        timer_end!(selected_result, 1);
        timer_start!(update_results);
        self.update_results();
        timer_end!(update_results, 1);
        #[cfg(feature="timing")]
        {
            BIGTIMER.with(|f| {
                let end_time = time::SteadyTime::now();
                println!("Total elapsed is {}ms", (end_time-*f.borrow()).num_milliseconds());
            });
        }
        timer_end!(timer_tick);

        0
    }

    fn clip_selected_result(&mut self) {
        if self.result_buffer.len() == 0 {
            self.selected_result = 0;
        } else if self.selected_result > self.result_buffer.len() - 1 {
            self.selected_result = self.result_buffer.len() - 1;
            self.result_dirty = true;
        }
    }

    // We defer plugin init because plugins may run arbitrarily blocking code
    // upon init (spinning up threads, initialising structures, etc), and we'd
    // very much like to minimise time-to-gui.
    fn deferred_plugin_init(&mut self) {
        timer_start!(deferred_plugin_init);
        let querier = FilesystemQuerier::new(&self.paths,
                                             self.config.search.settings_priority,
                                             self.config.search.subquery_penalty,
                                             self.config.search.subquery_path_penalty,
                                             self.config.search.subquery_accum_penalty,
                                             self.single_dir_mode,
                                             self.config.search.filesystem_threads as usize,
                                             self.config.search.throttle_regularity,
                                             std::env::current_exe().unwrap().as_path().with_file_name("cache.dat"),
                                             self.config.search.throttle_duration);
        self.queriers.push(PluginHolder {
            plugin: PluginTypes::Filesearch(querier),
            last_results: Vec::with_capacity(self.config.results.max_displayed),
            result_dirty: true,
        });
        timer_end!(deferred_plugin_init);
    }

    fn get_fresh_results(&mut self) {
        if self.queriers.len() == 0 {
            self.deferred_plugin_init();
        }

        let query = self.inputbox.get_contents();

        if query == "" && self.last_query == "" {
            for querier in &mut self.queriers {
                querier.plugin.update(UPDATE_TIMEOUT + self.first_update);
                self.first_update = 0;
            }
            return;
        } else if query == "" && self.last_query != "" {
            self.result_buffer.clear();
            self.result_dirty = true;
            self.last_query = query.into_owned();
            return;
        }

        // Note that this is valid; there is no priority
        // overlap between plugins; if a plugin is earlier than
        // another, all of its results should be displayed first.
        let max_results = self.config.results.max_displayed;
        let mut num_results = 0;
        for querier in &mut self.queriers {
            let cache_changed = querier.plugin.update(UPDATE_TIMEOUT + self.first_update);
            self.first_update = 0;
            if query != self.last_query || cache_changed {
                querier.result_dirty = true;
                let mut res = querier.plugin.search(&query, max_results);
                res.truncate(max_results);
                querier.last_results = res;
            } else {
                querier.result_dirty = false;
            }

            if num_results < max_results {
                if querier.result_dirty || self.result_dirty {
                    self.result_dirty = true;
                    let slice_end = std::cmp::min(max_results - num_results,
                                                  querier.last_results.len());
                    self.result_buffer.truncate(num_results);
                    self.result_buffer.extend_from_slice(&querier.last_results[..slice_end]);
                }
                num_results += querier.last_results.len();
            }
        }

        self.last_query = query.into_owned();
    }

    fn update_results(&mut self) {
        if !self.result_dirty {
            return;
        }

        let mut container_update = false;
        let mut dialog_update = false;
        if (self.result_buffer.len() > 0) &&
           (self.resbox_container.get_str_attribute("VISIBLE") == "NO") {
            container_update = true;
            dialog_update = true;
        } else if self.result_buffer.len() == 0 {
            let mut subcont = self.resbox_container
                                  .get_child(0)
                                  .expect("Error during GUI update.");
            true_hide(&mut subcont, self.config.input.width * 4);
            true_hide(&mut self.resbox_container, self.config.input.width * 4);
            self.resbox_container.refresh();
            self.result_dirty = false;
            return;
        }

        for (i, rbox) in self.resboxes.iter_mut().enumerate() {
            let hbox = rbox.get_child(0).as_hbox().expect("Error during GUI update.");
            let mut big = hbox.get_child(0).as_label().expect("Error during GUI update.");
            let mut small = hbox.get_child(1).as_label().expect("Error during GUI update.");
            if i < self.result_buffer.len() {
                let command = match self.result_buffer[i].command {
                    None => "".to_owned(),
                    Some(ref c) => format!("({})", c),
                };
                let mut bigchanged = big.get_label() != self.result_buffer[i].displayname;
                let mut smallchanged = small.get_label() != command;

                let targetbgcolor;
                let targetfgcolor;
                let targetbigfont;
                let targetsmallfont;
                if i == self.selected_result {
                    targetbgcolor = &self.config.results.selected_background;
                    targetfgcolor = &self.config.results.selected_foreground;
                    targetbigfont = &self.selected_result_font;
                    targetsmallfont = &self.selected_result_detail_font;
                } else {
                    targetbgcolor = &self.config.results.background;
                    targetfgcolor = &self.config.results.foreground;
                    targetbigfont = &self.result_font;
                    targetsmallfont = &self.result_detail_font;
                }
                if &rbox.get_str_attribute("BGCOLOR") != targetbgcolor {
                    rbox.set_str_attribute("BGCOLOR", targetbgcolor);
                    bigchanged = true;
                    smallchanged = true;
                }
                if &rbox.get_str_attribute("FGCOLOR") != targetfgcolor {
                    big.set_str_attribute("FGCOLOR", targetfgcolor);
                    small.set_str_attribute("FGCOLOR", targetfgcolor);
                    rbox.set_str_attribute("FGCOLOR", targetfgcolor);
                    small.set_str_attribute("FGCOLOR", targetfgcolor);
                    bigchanged = true;
                    smallchanged = true;
                }
                if &big.get_str_attribute("FONT") != targetbigfont {
                    big.set_str_attribute("FONT", targetbigfont);
                    bigchanged = true;
                    smallchanged = true;
                }
                if &small.get_str_attribute("FONT") != targetsmallfont {
                    small.set_str_attribute("FONT", targetsmallfont);
                    bigchanged = true;
                    smallchanged = true;
                }

                if bigchanged {
                    // Why does IUP do this. What in the name of crap.
                    big.set_label(&str::replace(&self.result_buffer[i].displayname, "&", "&&"));
                }
                if smallchanged {
                    small.set_label(&str::replace(&command, "&", "&&"));
                }
                if smallchanged || bigchanged {
                    if rbox.get_str_attribute("VISIBLE") == "NO" {
                        true_show(rbox, self.config.input.width * 4);
                        dialog_update = true;
                    }
                    rbox.refresh_children();
                    container_update = true;
                }
            } else {
                if rbox.get_str_attribute("VISIBLE") == "YES" {
                    big.set_label("");
                    small.set_label("");
                    true_hide(rbox, self.config.input.width * 4);
                    rbox.refresh_children();
                    container_update = true;
                    dialog_update = true;
                }
            }
        }

        if dialog_update {
            let mut subcont = self.resbox_container
                                  .get_child(0)
                                  .expect("Error during GUI update.");
            true_show(&mut subcont, self.config.input.width * 4);
            true_show(&mut self.resbox_container, self.config.input.width * 4);
            self.resbox_container.refresh();
        } else if container_update {
            let mut subcont = self.resbox_container
                                  .get_child(0)
                                  .expect("Error during GUI update.");
            true_show(&mut subcont, self.config.input.width * 4);
            true_show(&mut self.resbox_container, self.config.input.width * 4);
            self.resbox_container.refresh_children();
        }

        self.result_dirty = false;
    }

    // In certain specific conditions, it is possible
    // for IUP (and other GUI toolkits) to miss an unfocus
    // event or to start unfocused -- at least on Windows.
    // To work around this, we occasionally poll the focus state instead.
    // ACTIVEWINDOW and basically any method to find the window focus in IUP
    // did not work as well as this.
    fn check_self_focused(&mut self) -> bool {
        #[cfg(windows)]
        unsafe {
            GetForegroundWindow() == (self.window.get_raw_attr("HWND") as HWND)
        }
        #[cfg(not(windows))]
        true
    }
}


fn true_hide<T>(iw: &mut T, max_width: usize)
    where T: iup::IupWidget
{
    let mut dialog = iw.get_dialog();
    iw.set_str_attribute("FLOATING", "IGNORE");
    iw.set_str_attribute("VISIBLE", "NO");
    dialog.set_str_attribute("SIZE", &format!("{}x", max_width));
}

fn true_show<T>(iw: &mut T, max_width: usize)
    where T: iup::IupWidget
{
    let mut dialog = iw.get_dialog();
    iw.set_str_attribute("FLOATING", "NO");
    iw.set_str_attribute("VISIBLE", "YES");
    dialog.set_str_attribute("SIZE", &format!("{}x", max_width));
}

extern "C" fn loopout(_: *mut iup_sys::Ihandle) -> i32 {
    unsafe {
        iup_sys::IupExitLoop();
        0
    }
}

#[cfg(windows)]
extern "C" {
    fn DuplicateTokenEx(hExistingToken: HANDLE,
                        dwDesiredAccess: DWORD,
                        lpTokenAttributes: LPSECURITY_ATTRIBUTES,
                        ImpersonationLevel: DWORD,
                        TokenType: DWORD,
                        phNewToken: PHANDLE) -> BOOL;
    fn CreateProcessWithTokenW(hToken: HANDLE,
                               dwLogonFlags: DWORD,
                               lpApplicationName: LPCWSTR,
                               lpCommandLine: LPWSTR,
                               dwCreationFlags: DWORD,
                               lpEnvironment: LPVOID,
                               lpCurrentDirectory: LPCWSTR,
                               lpStartupInfo: LPSTARTUPINFOW,
                               lpProcessInfo: LPPROCESS_INFORMATION) -> BOOL;
    fn AdjustTokenPrivileges(TokenHandle: HANDLE,
                             DisableAllPrivileges: BOOL,
                             NewState: PTOKEN_PRIVILEGES,
                             BufferLength: DWORD,
                             PreviousState: PTOKEN_PRIVILEGES,
                             ReturnLength: DWORD) -> BOOL;
    fn AllocateAndInitializeSid(pIdentifierAuthority: PSID_IDENTIFIER_AUTHORITY,
                                nSubAuthorityCount: BYTE,
                                dwSubAuthority0: DWORD,
                                dwSubAuthority1: DWORD,
                                dwSubAuthority2: DWORD,
                                dwSubAuthority3: DWORD,
                                dwSubAuthority4: DWORD,
                                dwSubAuthority5: DWORD,
                                dwSubAuthority6: DWORD,
                                dwSubAuthority7: DWORD,
                                pSid: *mut PSID) -> BOOL;
    fn CheckTokenMembership(TokenHandle: HANDLE,
                            SidToCheck: PSID,
                            IsMember: PBOOL) -> BOOL;
}

fn wstring(s: &str) -> Vec<u16> {
    use std::os::windows::ffi::OsStrExt;
    use std::ffi::OsStr;
    use std::iter::once;
    // Null-terminated
    OsStr::new(s).encode_wide().chain(once(0)).collect()
}

fn shell_execute(verb: &str, cmd: &str, params: &str, cwd: &str) -> HINSTANCE {
    use std::ptr::null_mut;
    let wideverb = wstring(verb);
    let widecmd = wstring(cmd);
    let wideparams = wstring(params);
    let widecwd = wstring(cwd);
    // wstrings will always be at *least* legnth 1 because they're null-terminated.
    let verbptr = if wideverb.len() > 1 {wideverb.as_ptr()} else {null_mut()};
    let cmdptr = if widecmd.len() > 1 {widecmd.as_ptr()} else {null_mut()};
    let paramsptr = if wideparams.len() > 1 {wideparams.as_ptr()} else {null_mut()};
    let cwdptr = if widecwd.len() > 1 {widecwd.as_ptr()} else {null_mut()};
    unsafe {
        ShellExecuteW(null_mut(), verbptr, cmdptr, paramsptr, cwdptr, 10)
    }
}

fn error(msg: &str) -> ! {
    let mut error_dialog;
    unsafe {
        error_dialog = iup::IupGenericWidget::from_handle(iup_sys::IupMessageDlg());
    }
    error_dialog.set_str_attribute("DIALOGTYPE", "ERROR");
    error_dialog.set_str_attribute("TITLE", "Roxo Error");
    error_dialog.set_str_attribute("VALUE",
                                   &format!("Roxo encountered a fatal error:\n{}", msg));
    unsafe {
        iup_sys::IupPopup(error_dialog.get_handle(),
                          iup_sys::IUP_CENTER as i32,
                          iup_sys::IUP_CENTER as i32);
    }
    drop(error_dialog);
    println!("Error: {}", msg);
    std::process::exit(1);
}

#[cfg(windows)]
fn launch_file(command: &str,
               parameters: &str,
               workingdir: &str,
               escalate: bool) -> bool {
    // Windows API docs told me to do this.
    unsafe {
        CoInitializeEx(std::ptr::null_mut(), COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
    }
    let mut shret = 0x0 as HINSTANCE;
    if !escalate {
        shret = shell_execute("open", command, parameters, workingdir);
    }
    if escalate || shret == 0x5 as HINSTANCE {
        // Access denied. Most likely an exe or some kind of executable.
        // Let's try to run the file as an admin, see if we are successful.
        shret = shell_execute("runas", command, parameters, workingdir);
    }

    // The return type can be an arbitrarily large value, and HINSTANCE is
    // technically a signed int, so we have to check for underflow.
    if shret <= 32 as HINSTANCE && shret > 0 as HINSTANCE {
        if shret == ERROR_FILE_NOT_FOUND as HINSTANCE || shret == ERROR_PATH_NOT_FOUND as HINSTANCE {
            return false;
        }
        error(&format!("Failed to execute process: Error {:?}", shret));
    }

    true
}

#[cfg(not(windows))]
fn launch_file(command: &str, parameters: &str, workdir: &str, escalate: bool) -> bool {
    std::process::Command::new("sh")
        .arg("-c")
        .arg(command)
        .arg(parameters)
        .spawn()
        .expect("failed to execute process");
    return true
}

fn escalated_launch_file(command: &str, parameters: &str, workdir: &str) -> bool {
    launch_file(command, parameters, workdir, true)
}

fn deescalated_launch_file(command: &str, parameters: &str, workdir: &str) -> bool {
    use std::ptr::null_mut;
    // Check if our process has admin privileges and thus if we need to actually de-escalate at all.
    let mut adminsid: PSID;
    unsafe {
        adminsid = std::mem::uninitialized();
    }
    let mut nt_authority = SID_IDENTIFIER_AUTHORITY {Value: [0, 0, 0, 0, 0, 5]};
    unsafe {
        AllocateAndInitializeSid(&mut nt_authority,
                                 2,
                                 0x00000020u32, // SECURITY_BUILTIN_DOMAIN_RID from my winnt.h. Yes, I know.
                                 0x00000220u32, // DOMAIN_ALIAS_RID_ADMINS.
                                 0, 0, 0, 0, 0, 0,
                                 &mut adminsid);
    }
    let mut isadmin = 0;
    unsafe {
        CheckTokenMembership(null_mut(), adminsid, &mut isadmin);
    }
    if isadmin == 0 {
        return launch_file(command, parameters, workdir, false);
    }

    // Get the access token of our current process.
    // mem::uninintialized is v.v dangerous but was specifically made for FFI so, hey.
    let mut selftoken;
    unsafe {
        selftoken = std::mem::uninitialized();
        if OpenProcessToken(GetCurrentProcess(),
                            TOKEN_READ | TOKEN_ADJUST_PRIVILEGES,
                            &mut selftoken) == 0 {
            let longerr = GetLastError();
            error(&format!("Roxo could not get the current process token. winerror {}", longerr));
        }
    }

    // Set the increase memory quota privilege, which gives our
    // process the ability to launch a new process as another user.
    let privname: Vec<u16> = wstring("SeIncreaseQuotaPrivilege");
    let mut privluid;
    unsafe {
        privluid = std::mem::uninitialized();
        LookupPrivilegeValueW(null_mut(), privname.as_ptr(), &mut privluid);
    }
    let mut privs = TOKEN_PRIVILEGES {
        PrivilegeCount: 1,
        Privileges: [
            LUID_AND_ATTRIBUTES {
                Luid: privluid,
                Attributes: SE_PRIVILEGE_ENABLED
            }
        ]
    };
    unsafe {
        if AdjustTokenPrivileges(selftoken, 0, &mut privs, std::mem::size_of::<TOKEN_PRIVILEGES>() as u32, null_mut(), 0) == 0 {
            let longerr = GetLastError();
            error(&format!("Roxo could not set it's token privileges. winerror {}", longerr));
        }
    }

    // Grab the access token of the Shell process (usually explorer.exe)
    let shwnd;
    unsafe {
        shwnd = GetShellWindow();
    }
    let mut shpid;
    unsafe {
        shpid = std::mem::uninitialized();
        GetWindowThreadProcessId(shwnd, &mut shpid);
    }
    let shhandle;
    unsafe {
        shhandle = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, 0, shpid);
    }
    let mut shtoken;
    unsafe {
        shtoken = std::mem::uninitialized();
        if OpenProcessToken(shhandle, TOKEN_READ | TOKEN_DUPLICATE, &mut shtoken) == 0 {
            let longerr = GetLastError();
            error(&format!("Roxo could not get the Shell process token. winerror {}", longerr));
        }
    }

    // Duplicate the shell's access token
    let mut newtoken;
    unsafe {
        newtoken = std::mem::uninitialized();
        if DuplicateTokenEx(shtoken, MAXIMUM_ALLOWED, null_mut(), 3, 1, &mut newtoken) == 0 {
            let longerr = GetLastError();
            error(&format!("Roxo could not duplicate the Shell process token. winerror {}", longerr));
        }
    }

    // Spawn a new process with the duplicated access token, and therefore the
    // same access as the currently logged in user.
    let mut fudgestartupinfo: STARTUPINFOW;
    unsafe {
        fudgestartupinfo = std::mem::uninitialized();
        GetStartupInfoW(&mut fudgestartupinfo);
    }
    let mut widecommand: Vec<u16> = wstring(&format!("THROWAWAY {} {} {}",
                                                     escape(Cow::Borrowed(command)),
                                                     escape(Cow::Borrowed(workdir)),
                                                     escape(Cow::Borrowed(parameters))));
    let selfexe: Vec<u16> = wstring(std::env::current_exe().unwrap().to_str().unwrap());
    let procret: i32;
    let mut dumpstartupinfo: PROCESS_INFORMATION;
    unsafe {
        dumpstartupinfo = std::mem::uninitialized();
        procret = CreateProcessWithTokenW(newtoken,
                                          0,
                                          selfexe.as_ptr(),
                                          widecommand.as_mut_ptr(),
                                          0,
                                          null_mut(),
                                          null_mut(),
                                          &mut fudgestartupinfo,
                                          &mut dumpstartupinfo);
    }
    if procret == 0 {
        unsafe {
            let longerr = GetLastError();
            error(&format!("Roxo could not launch the de-escalated bootstrapper. winerror {}", longerr));
        }
    }

    unsafe {
        let exit_code: LPDWORD = std::mem::uninitialized();
        GetExitCodeProcess(dumpstartupinfo.hProcess, exit_code);
        *exit_code == STILL_ACTIVE
    }
}

fn bootstrap(command: Option<String>, workdir: Option<String>, args: String) {
    launch_file(&command.unwrap_or("".to_owned()), &args, &workdir.unwrap_or("".to_owned()), false);
}

fn extract_paths_from_config(config: &mut ConfigTop, paths: &mut Vec<Directory>) {
    for path in &mut config.search.directories {
        let depth = path.depth.unwrap_or(config.search.depth);
        // TAKE NOTE OF THIS LINE!
        // Config gets munged here.
        let stolen_filetypes = std::mem::replace(&mut path.filetypes, None);
        let filetypes = stolen_filetypes.unwrap_or(config.search.filetypes.clone());
        let stolen_blacklist = std::mem::replace(&mut path.blacklist, None);
        let blacklist = stolen_blacklist.unwrap_or(config.search.blacklist.clone());
        match shellexpand::full(&path.directory) {
            Ok(p) => {
                paths.push(Directory {
                    path: p.to_string(),
                    depth: depth,
                    filetypes: RegexSet::new(&filetypes)
                        .unwrap_or_else(|_| { error("Error while parsing filetypes, invalid regex") }),
                    blacklist: RegexSet::new(&blacklist)
                        .unwrap_or_else(|_| { error("Error while parsing blacklists, invalid regex") }),
                })
            }
            Err(_) => {}
        }
    }
    if config.search.use_path {
        let depth;
        if config.search.shallow_path {
            depth = 1;
        } else {
            depth = config.search.depth;
        }
        let mut ospaths = Vec::new();
        match env::var_os("PATH") {
            Some(p) => {
                ospaths.extend(env::split_paths(&p).map(|s| {
                    Directory {
                        path: s.to_str().unwrap().to_owned(),
                        depth: depth,
                        filetypes: RegexSet::new(&config.search.filetypes)
                            .unwrap_or_else(|_| { error("Error while parsing filetypes, invalid regex") }),
                        blacklist: RegexSet::new(&config.search.blacklist)
                            .unwrap_or_else(|_| { error("Error while parsing blacklists, invalid regex") }),
                    }
                }));
            }
            None => {}
        }
        if config.search.path_priority + 1 > (paths.len() as i64) ||
           config.search.path_priority < 0 {
            paths.append(&mut ospaths);
        } else {
            // It would be more obvious to loop over ospaths and use .insert()
            // to put the elements at the correct position within paths, but
            // one insertion is O(n) time. Letting Rust optimise memory access
            // by splitting things up and using .append() is way, way faster.
            let mut right = paths.split_off(config.search.path_priority as usize);
            paths.append(&mut ospaths);
            paths.append(&mut right);
        }
    }
}

fn main() {
    #[cfg(feature="timing")]
    BIGTIMER.with(|f| *f.borrow_mut() = time::SteadyTime::now());
    timer_start!(init);
    timer_start!(iup_open);
    // Unfortunately, we have to Open here rather than in UI to let error boxes work.
    unsafe {
        iup_sys::IupOpen(std::ptr::null_mut(), std::ptr::null_mut());
    }
    timer_end!(iup_open);

    // A shortcut to let Roxo run itself as a bootstrapper, or instead start
    // from a specific directory instead of using the config dirs.
    let mut args: std::env::Args = std::env::args();
    let newstart: String;
    if args.len() > 1 {
        // We make sure the first argument is either the executable name, or a
        // throwaway arg. This is for short program names such as ms-settings:
        // which have a null working directory.
        args.next();
        let command = args.next();
        let workdir = args.next();
        let commandargs = args.collect::<Vec<String>>().join(" ");
        if command.as_ref().unwrap() == "--start_dir" {
            newstart = workdir.unwrap();
        } else {
            bootstrap(command, workdir, commandargs);
            return;
        }
    } else {
        newstart = "".to_string();
    }

    timer_start!(config_open);
    let mut paths: Vec<Directory> = Vec::new();
    let mut configpath = std::env::current_exe().expect("Error while finding executable location.");
    configpath.set_file_name("config.yaml");
    let f = std::fs::File::open(configpath).unwrap_or_else(|_| {
        error("config.yaml not found!\nPlease place a config file in the same directory as the \
               executable.")
    });
    timer_end!(config_open);

    timer_start!(config_parse);
    let mut config: ConfigTop = serde_yaml::from_reader(&f).unwrap_or_else(|i| {
        error(&format!("Invalid config file found.\n{:?}", i))
    });
    timer_end!(config_parse);

    timer_start!(config_process);
    let single_dir_mode;
    if newstart == "" {
        extract_paths_from_config(&mut config, &mut paths);
        single_dir_mode = false;
    } else {
        let depth = 999;
        let filetypes = RegexSet::new(&[".*"]).unwrap();
        let blacklist = RegexSet::new(&config.search.folder_mode_blacklist).unwrap();
        paths.push(Directory {
            path: newstart,
            depth: depth,
            filetypes: filetypes,
            blacklist: blacklist
        });
        single_dir_mode = true
    }
    timer_end!(config_process);

    timer_end!(init);
    timer_start!(setup);
    let mut ui = Ui::setup(paths, config, single_dir_mode);
    timer_end!(setup);

    ui.launch();
}

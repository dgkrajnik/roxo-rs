extern crate walkdir;
extern crate ordered_float;
extern crate time;
extern crate crossbeam;
extern crate crossbeam_deque;
extern crate shellexpand;
extern crate winreg;
extern crate regex;
extern crate parking_lot;
extern crate bincode;

use cpl_consts;
use std;
use plugin::{Entry, Plugin};
use walkdir::{DirEntry, WalkDir};
use self::crossbeam_deque::{self as deque};
use self::parking_lot::{Mutex, Once, ONCE_INIT};
use std::collections::{BinaryHeap, HashSet};
use std::sync::{Arc, Weak};
use std::sync::atomic::{AtomicBool, Ordering};
use std::thread;
use std::time::SystemTime;
use std::path::PathBuf;
use std::fs::File;
use std::hash::{Hash, Hasher};
use regex::RegexSet;

macro_rules! ok_or_continue {
    ( $res:expr ) => {
        match $res {
            Ok(x) => x,
            _ => continue
        }
    };
}

#[cfg(feature="timing")]
use std::cell::RefCell;

#[cfg(feature="timing")]
thread_local!(static THREAD_TIMER: RefCell<Option<time::SteadyTime>> = RefCell::new(None));

// A measure of how important it is that a query is close to the start of the matching string.
const START_CLOSENESS_WEIGHT: i64 = 4;
// A measure of how important it is that the query is present in the matching string without gaps.
const UNBROKEN_RUN_WEIGHT: i64 = 4;

const SERIALIZATION_VERSION: i32 = 1;

static CACHE_TO_DISK: Once = ONCE_INIT;

#[derive(Debug, Hash, PartialEq, Eq, Clone, Serialize, Deserialize)]
struct CacheEntry {
    priority: usize,
    last_modified: SystemTime,
    extpriority: usize,
    parameters: Option<String>,
    entry: String,
    lowerentry: String,
    matchfield: String,
    lowermatchfield: String,
}

#[derive(Debug, Clone)]
struct HashCacheEntry(CacheEntry);

impl PartialEq for HashCacheEntry {
    fn eq(&self, other: &HashCacheEntry) -> bool {
        self.0.priority == other.0.priority && 
        self.0.extpriority == other.0.extpriority &&
        self.0.parameters == other.0.parameters &&
        self.0.entry == other.0.entry &&
        self.0.lowerentry == other.0.lowerentry &&
        self.0.matchfield == other.0.matchfield &&
        self.0.lowermatchfield == other.0.lowermatchfield
    }
}
impl Eq for HashCacheEntry {}
impl Hash for HashCacheEntry {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.priority.hash(state);
        self.0.extpriority.hash(state);
        self.0.parameters.hash(state);
        self.0.entry.hash(state);
        self.0.lowerentry.hash(state);
        self.0.matchfield.hash(state);
        self.0.lowermatchfield.hash(state);
    }
}

#[derive(Debug, Clone)]
pub struct Directory {
    pub path: String,
    pub depth: usize,
    pub filetypes: RegexSet,
    pub blacklist: RegexSet,
}
// A measure of how important it is that the query is present in the matching string without gaps.
impl PartialEq for Directory {
    fn eq(&self, other: &Directory) -> bool {
        self.path == other.path && self.depth == other.depth
    }
}

impl Ord for CacheEntry {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        (self.extpriority, self.priority, &self.entry)
            .cmp(&(other.extpriority, other.priority, &other.entry))
    }
}

impl PartialOrd for CacheEntry {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

pub struct FilesystemQuerier {
    active_cache: Arc<Mutex<HashSet<HashCacheEntry>>>,
    thread_cache: Arc<Mutex<Vec<CacheEntry>>>,
    last_cache_len: usize,
    subquery_penalty: i64,
    subquery_path_penalty: i64,
    subquery_accum_penalty: i64,
    single_dir_mode: bool,
    cache_path: PathBuf,
    force_update: Arc<AtomicBool>,
    _threads: Vec<thread::Thread>,
    _thread_aliveness: Weak<AtomicBool>
}

impl FilesystemQuerier {
    pub fn new(paths: &[Directory],
               settings_priority: i64,
               subquery_penalty: i64,
               subquery_path_penalty: i64,
               subquery_accum_penalty: i64,
               single_dir_mode: bool,
               num_threads: usize,
               throttle_regularity: u64,
               cache_path: PathBuf,
               throttle_duration: u64) -> FilesystemQuerier {
        let (folder_deque_worker, folder_deque_stealer) = deque::fifo();
        for (i, prioritised_path) in paths.iter().enumerate() {
            folder_deque_worker.push((i, prioritised_path.clone()));
        }

        let active_cache = Arc::new(Mutex::new(HashSet::new()));
        let thread_cache = Arc::new(Mutex::new(Vec::new()));
        let thread_alive = Arc::new(AtomicBool::new(true));
        let force_update = Arc::new(AtomicBool::new(false));

        let adjusted_settings_priority;
        if settings_priority + 1 > (paths.len() as i64) ||
           settings_priority < 0 {
            adjusted_settings_priority = paths.len() + 1;
        } else {
            adjusted_settings_priority = settings_priority as usize;
        }

        let mut num_threads = num_threads;
        if num_threads <= 1 {
            num_threads = 2;
        }

        #[cfg(feature="timing")]
        THREAD_TIMER.with(|f| *f.borrow_mut() = Some(time::SteadyTime::now()));

        let mut threads = Vec::with_capacity(num_threads-1);
        for i in 0..num_threads-1 {
            let fds = folder_deque_stealer.clone();
            let active_cache = active_cache.clone();
            let thread_cache = thread_cache.clone();
            let thread_alive = thread_alive.clone();
            threads.push(thread::Builder::new()
                             .name(format!("Cache-Producer {}", i))
                             .spawn(move || {
                                 // A reimplementation of the trick that test::black_box
                                 // does to prevent LLVM from optimising stuff out.
                                 unsafe { asm!("" : : "r"(&thread_alive)) }
                                 let _ta = thread_alive;
                                 do_thread(fds,
                                           thread_cache,
                                           active_cache,
                                           throttle_regularity,
                                           throttle_duration,
                                           single_dir_mode);
                             })
                             .unwrap()
                             .thread()
                             .clone());
        }
        if !single_dir_mode {
            {
            let thread_cache = thread_cache.clone();
            let thread_alive = thread_alive.clone();
            let active_cache = active_cache.clone();
            thread::Builder::new()
                .name(format!("Control-Producer {}", 1))
                .spawn(move || {
                    unsafe { asm!("" : : "r"(&thread_alive)) }
                    let _ta = thread_alive;
                    do_cpl_thread(thread_cache, active_cache, adjusted_settings_priority);
                })
                .unwrap()
                .thread();
            }
            {
            let thread_alive = thread_alive.clone();
            let active_cache = active_cache.clone();
            let cache_path = cache_path.clone();
            thread::Builder::new()
                .name(format!("Disk-Producer {}", 1))
                .spawn(move || {
                    unsafe { asm!("" : : "r"(&thread_alive)) }
                    let _ta = thread_alive;
                    do_disk_read_thread(active_cache, cache_path);
                })
                .unwrap()
                .thread();
            }
        }
        let fq = FilesystemQuerier {
            thread_cache: thread_cache,
            active_cache: active_cache,
            last_cache_len: 0,
            subquery_penalty: subquery_penalty,
            subquery_path_penalty: subquery_path_penalty,
            subquery_accum_penalty: subquery_accum_penalty,
            single_dir_mode: single_dir_mode,
            cache_path: cache_path,
            force_update: force_update,
            _threads: threads,
            _thread_aliveness: Arc::downgrade(&thread_alive),
        };
        fq
    }
}

impl Plugin for FilesystemQuerier {
    fn update(&mut self, timeout: u64) -> bool {
        match self._thread_aliveness.upgrade() {
            Some(_) => {}, //Threads are still alive
            None => { //Threads are all dead
                #[cfg(feature="timing")]
                THREAD_TIMER.with(|f| {
                    match *f.borrow_mut() {
                        Some(threads_ended) => timer_print!(threads_ended, 5),
                        None => {}
                    }
                    *f.borrow_mut() = None;
                });
                if !self.single_dir_mode {
                    CACHE_TO_DISK.call_once(|| {
                        let active_cache = self.active_cache.clone();
                        let thread_cache = self.thread_cache.clone();
                        let force_update = self.force_update.clone();
                        let cache_path = self.cache_path.clone();
                        thread::Builder::new().spawn(|| {
                            do_disk_write_thread(active_cache, thread_cache, force_update, cache_path);
                        }).unwrap();
                    });
                }
            }
        }
        let timeout = std::time::Duration::from_millis(timeout);
        let cache = match self.active_cache.try_lock_for(timeout) {
            Some(x) => x,
            None => return true
        };
        let force = self.force_update.load(Ordering::Relaxed);
        if cache.len() != self.last_cache_len || force {
            self.last_cache_len = cache.len();
            self.force_update.store(false, Ordering::Relaxed);
            true
        } else {
            false
        }
    }

    fn search(&self, query: &str, max_results: usize) -> Vec<Entry> {
        // This one is a bit of a doozy.
        // We have a rating system that means a larger number is a better match.
        // We would like to keep track of only the largest ratings we find, BUT we
        // would also like an efficient mechanism to eject the *smallest* rating.
        // To this end, we invert the ordering via negation so now the head of the
        // heap is the value closest to 0 (worst rank) and we can easily eject it.
        let mut heap = BinaryHeap::new();
        let query = query.to_lowercase();
        let mut biggest = 0;
        timer_start!(cachegen);
        let mut seen_commands = HashSet::new();
        let mut cache: Vec<CacheEntry>;
        // We do all this nonsense to:
        //     - Try to reduce the time search() spends with a lock.
        //     - Remove the randomness from the HashSet insertions.
        //
        {
            let c = self.active_cache.lock();
            cache = (*c).iter().map(|x| x.0.clone()).collect();
        }
        cache.sort_unstable_by(|a, b| a.lowermatchfield.cmp(&b.lowermatchfield));
        for entry in cache.drain(..) {
            let rating = full_rate_match(&query,
                                         &entry,
                                         self.subquery_penalty,
                                         self.subquery_path_penalty,
                                         self.subquery_accum_penalty);
            if rating == 0 {continue}

            let merged_command = format!("{} {}", &entry.entry, &entry.parameters.as_ref().unwrap_or(&"".to_string()));
            if !seen_commands.insert(merged_command) {continue}

            let rating = -rating;
            if heap.len() < max_results {
                heap.push((rating, entry));
                biggest = heap.peek().unwrap().0;
            } else if rating <= biggest {
                // This is the syntax for a semantic fast pop_push... for some reason.
                let mut head = heap.peek_mut().unwrap();
                *head = (rating, entry);
                biggest = head.0
            }
        }
        timer_end!(cachegen, 2);

        // This will output in ascending order. Since we've inverted the ordering,
        // this means that the result furthest from 0 (i.e the best rank) will
        // be the first in the sorted vector, and the one closest to 0 the last.
        timer_start!(vecgen);
        let j = heap.into_sorted_vec()
            .iter()
            .map(|e| {
                let displayname = e.1.matchfield.to_owned();
                let command = &e.1.entry;
                Entry {
                    displayname: displayname,
                    command: Some(command.trim_left_matches(r"\\?\").to_owned()),
                    parameters: e.1.parameters.to_owned(),
                }
            })
            .collect();
        timer_end!(vecgen, 2);

        j
    }
}

fn do_cpl_thread(thread_cache: Arc<Mutex<Vec<CacheEntry>>>,
                 active_cache: Arc<Mutex<HashSet<HashCacheEntry>>>,
                 settings_priority: usize) {
    let start_time = SystemTime::now();

    let mut compact_entries = Vec::new();
    let mut seen_cnames: HashSet<String> = HashSet::new();
    let control_executable = shellexpand::full("${SystemRoot}\\SysWOW64\\control.exe").unwrap().to_string();
    let hkcr = winreg::RegKey::predef(winreg::enums::HKEY_CLASSES_ROOT);
    let clsid = hkcr.open_subkey_with_flags("CLSID", winreg::enums::KEY_READ).unwrap();
    // Process hardcoded ms-setting: uris.
    for &(setting_name, setting_uri) in cpl_consts::MS_SETTINGS.iter() {
        let matchfield = format!("{} (Windows Settings)", setting_name);
        compact_entries.push((None, Some(setting_uri.to_string()), matchfield));
    }
    // Process hardoded ms-setting entries.
    for &(setting_name, setting_cname) in cpl_consts::CPL_NAMES.iter() {
        seen_cnames.insert(setting_cname.to_owned());
        let matchfield = format!("{} (Control Panel)", setting_name);
        compact_entries.push((Some(setting_cname.to_owned()), None, matchfield));
    }
    // Scan the registry for more canonical names, such as those for control panels
    // installed by apps.
    for cls in clsid.enum_keys().map(|x| x.unwrap()) {
        let subkey = ok_or_continue!(clsid.open_subkey_with_flags(cls, winreg::enums::KEY_READ));
        let setting_cname: String = ok_or_continue!(subkey.get_value("System.ApplicationName"));
        let setting_name: String = ok_or_continue!(subkey.get_value(""));
        if seen_cnames.contains(&setting_cname) {continue};
        let matchfield = format!("{} (Control Panel)", setting_name);
        compact_entries.push((Some(setting_cname.to_owned()), None, matchfield));
    }

    for (setting_cname, entry, matchfield) in compact_entries {
        let cache_entry = CacheEntry {
            priority: settings_priority,
            extpriority: 0,
            last_modified: start_time,
            parameters: setting_cname.and_then(|x| Some(format!("/name {}", x))),
            entry: entry.unwrap_or(control_executable.clone()),
            lowerentry: matchfield.to_lowercase(),
            matchfield: matchfield.to_owned(),
            lowermatchfield: matchfield.to_lowercase(),
        };

        {
            let mut thread_cache = thread_cache.lock();
            thread_cache.push(cache_entry.clone());
            let mut active_cache = active_cache.lock();
            active_cache.replace(HashCacheEntry(cache_entry));
        }
    }
}

fn has_extension(entry: &DirEntry, extension_regex: &RegexSet) -> bool {
    match entry.path().extension() {
        Some(ext) => extension_regex.is_match(&ext.to_string_lossy()),
        None => extension_regex.is_match(""),
    }
}

fn do_thread(paths: deque::Stealer<(usize, Directory)>,
             thread_cache: Arc<Mutex<Vec<CacheEntry>>>,
             active_cache: Arc<Mutex<HashSet<HashCacheEntry>>>,
             throttle_regularity: u64,
             throttle_duration: u64,
             include_extension: bool) {
    let throttle_dur = std::time::Duration::from_millis(throttle_duration);

    'treewalk: loop {
        let (priority, path) = match paths.steal() {
            deque::Steal::Data(x) => x,
            deque::Steal::Retry => continue,
            deque::Steal::Empty => break 'treewalk,
        };

        let depth = path.depth;
        let mut step = 0;
        let walker = WalkDir::new(&path.path)
                         .min_depth(0)
                         .max_depth(depth)
                         .into_iter()
                         .filter_entry(|ref e| e.depth() == 1 ||
                                               !path.blacklist.is_match(&e.path().to_string_lossy()))
                         .filter_map(|e| {
                             if step >= throttle_regularity {
                                 thread::sleep(throttle_dur);
                                 step = 0;
                             }
                             step += 1;
                             e.ok()
                         })
                         .filter(|ref e| has_extension(e, &path.filetypes));
        for entry in walker {
            let fpath = ok_or_continue!(entry.path().canonicalize());
            let extpriority = match fpath.extension() {
                Some(e) => path.filetypes.matches(&e.to_string_lossy()).iter().next().unwrap_or(9999),
                None => path.filetypes.matches("").iter().next().unwrap_or(9999)
            };
            let displayname = match include_extension {
                true => fpath.file_name(),
                false => fpath.file_stem()
            }.unwrap().to_string_lossy().into_owned();
            let fentry = fpath.to_str().unwrap().to_string();
            let cache_entry = CacheEntry {
                priority: priority,
                extpriority: extpriority,
                last_modified: entry.metadata().unwrap().modified().unwrap(),
                parameters: None,
                lowerentry: fentry.to_lowercase(),
                entry: fentry,
                lowermatchfield: displayname.to_lowercase(),
                matchfield: displayname,
            };

            {
                let mut thread_cache = thread_cache.lock();
                thread_cache.push(cache_entry.clone());
                let mut active_cache = active_cache.lock();
                active_cache.replace(HashCacheEntry(cache_entry));
            }
        }
    }
}

fn do_disk_read_thread(active_cache: Arc<Mutex<HashSet<HashCacheEntry>>>,
                       cache_path: PathBuf) {
    // Don't let the deserializer use more than 100MB.
    // If it does, something *probably* went wrong.
    let mut deserializer = bincode::config();
    deserializer.limit(100*1024*1024);
    let mut disk_cache = std::fs::File::open(&cache_path).and_then(|f| {
        // If the cache is invalid, we can just silently fail since we can rebuild it.
        let dcache: (i32, Vec<CacheEntry>) = deserializer.deserialize_from(&f).unwrap_or((-1, Vec::new()));
        match dcache.0 {
            SERIALIZATION_VERSION => Ok(dcache.1),
            _ => Err(std::io::Error::new(std::io::ErrorKind::InvalidData ,""))
        }
    }).unwrap_or_else(|_| {
        Vec::new()
    });

    for entry in disk_cache.drain(..) {
        let l = HashCacheEntry(entry);
        {
            let mut cache = active_cache.lock();
            if !cache.contains(&l) {
                cache.insert(l);
            }
        }
    }
}

fn do_disk_write_thread(active_cache: Arc<Mutex<HashSet<HashCacheEntry>>>,
                        thread_cache: Arc<Mutex<Vec<CacheEntry>>>,
                        force_update: Arc<AtomicBool>,
                        cache_path: PathBuf) {
    // If there is a failure with file creation, we don't *really* care.
    // We ignore since we are using the thread cache anyway.
    let mut tcache;
    {
        let cache = thread_cache.lock();
        tcache = (SERIALIZATION_VERSION, cache.clone())
    }
    File::create(&cache_path).and_then(|f| {
        bincode::serialize_into(f, &tcache).unwrap_or(());
        Ok(())
    }).unwrap_or(());
    let new_active: HashSet<HashCacheEntry> = tcache.1.drain(..).map(|x| { HashCacheEntry(x) }).collect();
    {
        let mut cache = active_cache.lock();
        (*cache) = new_active;
        cache.shrink_to_fit();
        force_update.store(true, Ordering::Relaxed);
    }
}

#[inline]
fn full_rate_match(query: &str, entry: &CacheEntry,
                   subquery_penalty: i64, subquery_path_penalty: i64, subquery_accum_penalty: i64) -> i64 {
    let lowermatch = &entry.lowermatchfield;
    let truerating = rate_match(lowermatch, &query);
    if truerating > 0 {
        truerating
    } else {
        let mut accum = 0;
        let mut seen_subqueries: HashSet<String> = HashSet::new();
        for subquery in query.split_whitespace() {
            if !seen_subqueries.insert(subquery.to_owned()) {
                continue;
            }
            let matchrating = rate_match(lowermatch, &subquery)/subquery_penalty;
            let componentrating = component_rate_match(&entry.lowerentry, &subquery)/subquery_path_penalty;
            let accumrating = std::cmp::max(matchrating, componentrating);
            if accumrating == 0 {
                accum = 0;
                break;
            } else {
                accum += accumrating
            }
        }
        accum/subquery_accum_penalty
    }
}

// Larger number corresponds to a better match.
// While i64 doesn't really buy us a lot of speed, it does buy better ergonomics
// vs floats, since we don't have to contend with a lack of ordering, NaNs, etc.
#[inline]
fn rate_match(s1: &str, s2: &str) -> i64 {
    if s2.len() > s1.len() {
        return 0;
    }

    let mut matches: i64 = 0;
    let mut steps: i64 = 0;
    let mut start_pos: i64 = 0;
    let mut c2s = s2.bytes();
    let mut c2 = match c2s.next() {
        Some(c) => c,
        None => {
            return 0;
        }
    };

    for c1 in s1.bytes() {
        if c1 == c2 {
            matches += 1;
            c2 = match c2s.next() {
                Some(c) => c,
                None => {
                    break;
                }
            };
        } else {
            if matches > 0 {
                steps += 1;
            } else {
                start_pos += 1;
            }
        }
    }

    if matches < (s2.len() as i64) {
        0
    } else {
        // Magic number is 2^30. This serves to, essentially, make the output of rate_match
        // a fixed point number. Kind of.
        ((s2.len() as i64) * 1_073_741_824) /
            ((s1.len() as i64) * ((start_pos * START_CLOSENESS_WEIGHT) + (steps * UNBROKEN_RUN_WEIGHT) + 1))
    }
}

// A specialised version of rate_match for matching against path strings. Essentially,
// this efficiently 'splits' the path.
#[inline]
fn component_rate_match(s1: &str, s2: &str) -> i64 {
    if s2.len() > s1.len() {
        return 0;
    }

    let mut matches: i64 = 0;
    let mut steps: i64 = 0;
    let mut start_pos: i64 = 0;
    let mut c1p: i64 = 0;
    let mut accum: i64 = 0;
    let mut c2s = s2.bytes();
    let mut c2 = match c2s.next() {
        Some(c) => c,
        None => {
            return 0;
        }
    };

    let mut skip = false;
    for c1 in s1.bytes() {
        if c1 == c2 && !skip {
            matches += 1;
            c2 = match c2s.next() {
                Some(c) => c,
                None => {skip = true; continue}
            };
            c1p += 1;
        } else if c1 as char == std::path::MAIN_SEPARATOR { 
            if matches >= (s2.len() as i64) {
                let subaccum = ((s2.len() as i64) * 1_073_741_824) /
                    ((c1p+1) * ((start_pos * START_CLOSENESS_WEIGHT) + (steps * UNBROKEN_RUN_WEIGHT) + 1));
                accum = std::cmp::max(accum, subaccum);
            }
            matches = 0;
            steps = 0;
            start_pos = 0;
            c1p = 0;
            skip = false;
            c2s = s2.bytes();
            c2 = c2s.next().unwrap();
        } else if !skip {
            if matches > 0 {
                steps += 1;
            } else {
                start_pos += 1;
            }
            c1p += 1;
        }
    }

    accum
}

#[cfg(test)]
#[test]
fn rating_test() {
    let r1 = rate_match("banana", "ban");
    let r2 = rate_match("banana", "nan");
    let r3 = rate_match("banana", "ana");
    assert!(r1 > r2);
    assert!(r1 > r3);
    assert!(r3 > r2);

    let r4 = rate_match("kanban", "ban");
    assert!(r1 > r4);

    let r5 = rate_match("banana", "bana");
    assert!(r5 > r1);

    let r6 = rate_match("volcano", "olan");
    let r7 = rate_match("molecular node", "olan");
    assert!(r6 > r7);

    let r8 = rate_match("tolcan", "olan");
    assert!(r8 > r6);

    let r9 = rate_match("kanban", "nan");
    assert!(r4 > r9);

    let r10 = rate_match("molecular node", "de");
    let r11 = rate_match("molecular node", "ml");
    assert!(r11 > r10);

    let r12 = rate_match("gratuitous", "grt");
    let r13 = rate_match("great", "grt");
    assert!(r13 > r12);

    let r14 = rate_match("onion", "oin");
    let r15 = rate_match("oinon", "oin");
    let r16 = rate_match("oinabler", "oin");
    let r17 = rate_match("opinoin", "oin");
    assert!(r15 > r14);
    assert!(r15 > r16);
    assert!(r15 > r17);
    assert!(r16 > r14);
    assert!(r16 > r17);
    assert!(r17 > r14);
}

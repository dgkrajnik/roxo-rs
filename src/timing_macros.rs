#[macro_export]
macro_rules! timer_start {
    ( $timer_name:ident ) => {
        #[cfg(feature="timing")]
        let $timer_name = time::SteadyTime::now();
    }
}
#[macro_export]
macro_rules! timer_end {
    ( $timer_name:ident ) => {
        #[cfg(feature="timing")]
        {
            let _end_timer = time::SteadyTime::now();
            println!("{} in {}ms", stringify!($timer_name), (_end_timer-$timer_name).num_milliseconds());
        }
    };
    ( $timer_name:ident, $indent:expr ) => {
        #[cfg(feature="timing")]
        {
            let _end_timer = time::SteadyTime::now();
            println!("{:indent$}{} in {}ms", "", stringify!($timer_name), (_end_timer-$timer_name).num_milliseconds(), indent=$indent*4);
        }
    }
}

#[macro_export]
macro_rules! timer_print {
    ( $timer_name:ident, $indent:expr ) => {
        {
            let _end_timer = time::SteadyTime::now();
            println!("{:indent$}{} in {}ms", "", stringify!($timer_name), (_end_timer-$timer_name).num_milliseconds(), indent=$indent*4);
        }
    }
}


#[macro_export]
macro_rules! timer_accum_start {
    ( $timer_name:ident, $accum_name:ident ) => {
        #[cfg(feature="timing")]
        let mut $timer_name: i64 = 0;
    }
}
#[macro_export]
macro_rules! timer_accum_open {
    ( $timer_name:ident, $accum_name:ident ) => {
        #[cfg(feature="timing")]
        let $accum_name = time::SteadyTime::now();
    }
}
#[macro_export]
macro_rules! timer_accum_close {
    ( $timer_name:ident, $accum_name:ident ) => {
        #[cfg(feature="timing")]
        {
            let _end_timer = time::SteadyTime::now();
            $timer_name += (_end_timer-$accum_name).num_nanoseconds().unwrap();
        }
    }
}
#[macro_export]
macro_rules! timer_accum_end {
    ( $timer_name:ident, $accum_name:ident ) => {
        #[cfg(feature="timing")]
        {
            println!("{} in {}ms", stringify!($timer_name), $timer_name/1_000_000);
        }
    };
    ( $timer_name:ident, $accum_name:ident, $indent:expr ) => {
        #[cfg(feature="timing")]
        {
            println!("{:indent$}{} in {}ms", "", stringify!($timer_name), $timer_name/1_000_000, indent=$indent*4);
        }
    }
}

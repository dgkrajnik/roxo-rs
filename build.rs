#[cfg(windows)]
extern crate winres;

fn main() {
    add_resfile();
}

// Note that for winres to work you need to have windres, ac, and rc in your PATH.
// Per the winres readme, windres/ac can come from mingw64, and rc is in the Windows Kits.

#[cfg(windows)]
fn add_resfile() {
    // There is some sort of directory setting-related issue when windres attempts
    // to build resource files, so we build a complete path to the iup.rc
    let mut res_file = std::env::current_dir().unwrap();
    res_file.push("iup.rc");

    // A little note for those who end up here, inevitably exasperated: The version of windres that
    // comes with mingw64, for whatever reason, would fail to run for me when it was in a path with spaces in it.
    // Perhaps it interpreted the space in argv[0] as the next argument?
    let mut res = winres::WindowsResource::new();
    res.set_resource_file(res_file.to_str().unwrap());
    match res.compile() {
        Ok(_) => {},
        Err(e) => {
            println!("cargo:warning=Winres failed to run with error: {:?}", e);
        },
    }
}

#[cfg(unix)]
fn add_resfile() {}

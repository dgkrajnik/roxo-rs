#[cfg(windows)]
extern crate winapi;
#[cfg(windows)]
extern crate user32;
extern crate iup_sys;

use iup_sys::{Ihandle, Icallback};
use iup_sys::{IupSetAttribute, IupGetAttribute, IupSetAttributes};
use iup_sys::{IupSetStrAttribute, IupSetInt};
use iup_sys::{IupGetInt, IupGetIntInt};
use iup_sys::IupGetDialog;
use iup_sys::{IupHide, IupShow, IupShowXY, IupRefresh, IupRefreshChildren};
use iup_sys::{IupAppend, IupGetChild};
use iup_sys::{IupSetCallback, IupMap};

use std::borrow::Cow;

pub enum XPos {
    Left,
    Center,
    Right,
    Mousepos,
    Centerparent,
    Current,
    Coord(u32),
}
impl Into<u32> for XPos {
    fn into(self) -> u32 {
        match self {
            XPos::Left => iup_sys::IUP_LEFT,
            XPos::Center => iup_sys::IUP_CENTER,
            XPos::Right => iup_sys::IUP_RIGHT,
            XPos::Mousepos => iup_sys::IUP_MOUSEPOS,
            XPos::Centerparent => iup_sys::IUP_CENTERPARENT,
            XPos::Current => iup_sys::IUP_CURRENT,
            XPos::Coord(x) => x,
        }
    }
}
pub enum YPos {
    Top,
    Center,
    Bottom,
    Mousepos,
    Centerparent,
    Current,
    Coord(u32),
}
impl Into<u32> for YPos {
    fn into(self) -> u32 {
        match self {
            YPos::Top => iup_sys::IUP_TOP,
            YPos::Center => iup_sys::IUP_CENTER,
            YPos::Bottom => iup_sys::IUP_BOTTOM,
            YPos::Mousepos => iup_sys::IUP_MOUSEPOS,
            YPos::Centerparent => iup_sys::IUP_CENTERPARENT,
            YPos::Current => iup_sys::IUP_CURRENT,
            YPos::Coord(x) => x,
        }
    }
}

/// A trait which allows convenient casting of a rediscovered widget into an expected type.
pub trait RediscoveredCast {
    fn as_dialog(self) -> Result<IupDialog, Option<IupGenericWidget>>;
    fn as_label(self) -> Result<IupLabel, Option<IupGenericWidget>>;
    fn as_hbox(self) -> Result<IupHbox, Option<IupGenericWidget>>;
    fn as_vbox(self) -> Result<IupVbox, Option<IupGenericWidget>>;
    fn as_bbox(self) -> Result<IupBbox, Option<IupGenericWidget>>;
    fn as_text(self) -> Result<IupText, Option<IupGenericWidget>>;
    fn as_timer(self) -> Result<IupTimer, Option<IupGenericWidget>>;
    fn as_generic(self) -> Result<IupGenericWidget, Option<IupGenericWidget>>;
}

pub trait IupWidget {
    unsafe fn get_handle(&self) -> *mut Ihandle;

    fn set_str_attribute(&mut self, attribute: &str, value: &str) {
        unsafe {
            let ih = self.get_handle();
            let iupattr = to_ffi(attribute);
            let iupval = to_ffi(value);
            IupSetStrAttribute(ih, iupattr, iupval);
            // IUP internally duplicates these strings.
            free_ffi(iupattr);
            free_ffi(iupval);
        }
    }

    fn set_int_attribute(&mut self, attribute: &str, value: i32) {
        unsafe {
            let ih = self.get_handle();
            let iupattr = to_ffi(attribute);
            IupSetInt(ih, iupattr, value);
            free_ffi(iupattr);
        }
    }

    fn set_bool_attribute(&mut self, attribute: &str, value: bool) {
        unsafe {
            // According to the IUP documentation, this is idiomatic.
            let ih = self.get_handle();
            let iupattr = to_ffi(attribute);
            if value {
                IupSetInt(ih, iupattr, 1);
            } else {
                IupSetInt(ih, iupattr, 0);
            }
            free_ffi(iupattr);
        }
    }

    /// Set multiple attributes from a comma-separated k=v list.
    fn set_attributes(&mut self, kv: &str) {
        unsafe {
            // Note that IupSetAttributes will internally duplicate values
            // as it sees them, so we don't have to keep the string around.
            let ih = self.get_handle();
            let iupkv = to_ffi(kv);
            IupSetAttributes(ih, iupkv);
            free_ffi(iupkv);
        }
    }

    fn get_str_attribute(&self, attribute: &str) -> Cow<str> {
        unsafe {
            let ih = self.get_handle();
            let iupattr = to_ffi(attribute);
            let val = IupGetAttribute(ih, iupattr);
            let ret = std::ffi::CStr::from_ptr(val).to_string_lossy();
            free_ffi(iupattr);
            ret
        }
    }

    fn get_int_attribute(&self, attribute: &str) -> i32 {
        unsafe {
            let ih = self.get_handle();
            let iupattr = to_ffi(attribute);
            let ret = IupGetInt(ih, iupattr);
            free_ffi(iupattr);
            ret
        }
    }

    /// Get an attr that is comprised of two ints, like a WxH or X,Y.
    fn get_intint_attribute(&self, attribute: &str) -> (i32, i32) {
        unsafe {
            let ih = self.get_handle();
            let mut x = 0;
            let mut y = 0;
            let iupattr = to_ffi(attribute);
            IupGetIntInt(ih, iupattr, &mut x, &mut y);
            free_ffi(iupattr);
            (x, y)
        }
    }

    fn get_dialog(&self) -> IupDialog {
        let dialog;
        unsafe {
            dialog = IupGetDialog(self.get_handle());
        }
        IupDialog(dialog)
    }

    /// Refresh the entire dialog that this widget belongs to.
    fn refresh(&mut self) {
        unsafe {
            IupRefresh(self.get_handle());
        }
    }

    fn hide(&mut self) {
        unsafe {
            let ih = self.get_handle();
            IupHide(ih);
        }
    }

    fn show(&mut self) {
        unsafe {
            let ih = self.get_handle();
            IupShow(ih);
        }
    }

    fn set_callback(&mut self, action: &str, callback: Icallback) {
        unsafe {
            let iupaction = to_ffi(action);
            IupSetCallback(self.get_handle(), iupaction, callback);
            free_ffi(iupaction);
        }
    }

    /// Unsafely set an attr to a raw c pointer.
    unsafe fn set_raw_attr(&mut self, attr: &str, val: *const std::os::raw::c_char) {
        let iupattr = to_ffi(attr);
        IupSetAttribute(self.get_handle(), iupattr, val);
        free_ffi(iupattr);
    }

    /// Internal helper to get arbitrary attributes as a pointer.
    unsafe fn get_raw_attr(&self, attr: &str) -> *mut std::os::raw::c_char {
        let iupattr = to_ffi(attr);
        let ret = IupGetAttribute(self.get_handle(), iupattr);
        free_ffi(iupattr);
        ret
    }

    /// Get the Iup-side classname of the widget.
    fn get_class_name(&self) -> Cow<str> {
        unsafe {
            let iupcname = iup_sys::IupGetClassName(self.get_handle());
            std::ffi::CStr::from_ptr(iupcname).to_string_lossy()
        }
    }

    fn map(&self) {
        unsafe {
            IupMap(self.get_handle());
        }
    }

}

pub trait IupContainer : IupWidget {
    /// Append a child to the container.
    fn add_child<T>(&mut self, child: &mut T)
        where T: IupWidget
    {
        unsafe {
            IupAppend(self.get_handle(), child.get_handle());
        }
    }
    /// Append multiple children to the container.
    fn add_children<T>(&mut self, children: &mut Vec<T>)
        where T: IupWidget
    {
        for child in children {
            self.add_child(child);
        }
    }

    /// Get the child of the container at position `pos`.
    fn get_child(&self, pos: i32) -> Option<IupGenericWidget> {
        let ih;
        let child;
        unsafe {
            ih = self.get_handle();
            child = IupGetChild(ih, pos);
        }
        if child.is_null() {
            None
        } else {
            Some(IupGenericWidget(child))
        }
    }

    /// Refresh *only* the children of this container.
    ///
    /// IUP Says that this doesn't work on Dialogs.
    fn refresh_children(&mut self) {
        unsafe {
            IupRefreshChildren(self.get_handle());
        }
    }
}

#[macro_export]
macro_rules! with_children {
    ( $type:ty, [$( $child:expr ),*] ) => {{
        let mut parent = <$type>::new();
        $( parent.add_child(&mut $child); )*
        parent
    }}
}

#[macro_export]
macro_rules! with_attribs {
    ( $type:ty, $attribs:expr ) => {{
        let mut parent = <$type>::new();
        parent.set_attributes($attribs);
        parent
    }}
}

#[macro_export]
macro_rules! with_attribs_and_children {
    ( $type:ty, $attribs:expr, [$( $child:expr ),*] ) => {{
        let mut parent = <$type>::new();
        parent.set_attributes($attribs);
        $( parent.add_child(&mut $child); )*
        parent
    }}
}

/// Widget of unknown type and purpose; usually obtained directly from Iup
#[derive(Debug)]
pub struct IupGenericWidget(*mut Ihandle);
impl IupWidget for IupGenericWidget {
    unsafe fn get_handle(&self) -> *mut Ihandle {
        self.0
    }
}
impl IupGenericWidget {
    /// Convert from a raw Ihandle into a generic widget.
    ///
    /// # Safety
    ///
    /// If given an invalid Ihandle, the resulting GenericWidget will exhibit
    /// undefined behaviour. Be careful!
    pub unsafe fn from_handle(ih: *mut Ihandle) -> IupGenericWidget {
        IupGenericWidget(ih)
    }
}

impl RediscoveredCast for IupGenericWidget {
    fn as_dialog(self) -> Result<IupDialog, Option<IupGenericWidget>> {
        unsafe {
            match &(*self.get_class_name()) {
                "dialog" => Ok(IupDialog(self.get_handle())),
                _ => Err(Some(self)),
            }
        }
    }

    fn as_label(self) -> Result<IupLabel, Option<IupGenericWidget>> {
        unsafe {
            match &(*self.get_class_name()) {
                "label" => Ok(IupLabel(self.get_handle())),
                _ => Err(Some(self)),
            }
        }
    }

    fn as_hbox(self) -> Result<IupHbox, Option<IupGenericWidget>> {
        unsafe {
            match &(*self.get_class_name()) {
                "hbox" => Ok(IupHbox(self.get_handle())),
                _ => Err(Some(self)),
            }
        }
    }

    fn as_vbox(self) -> Result<IupVbox, Option<IupGenericWidget>> {
        unsafe {
            match &(*self.get_class_name()) {
                "vbox" => Ok(IupVbox(self.get_handle())),
                _ => Err(Some(self)),
            }
        }
    }

    fn as_bbox(self) -> Result<IupBbox, Option<IupGenericWidget>> {
        unsafe {
            match &(*self.get_class_name()) {
                "bbox" => Ok(IupBbox(self.get_handle())),
                _ => Err(Some(self)),
            }
        }
    }

    fn as_text(self) -> Result<IupText, Option<IupGenericWidget>> {
        unsafe {
            match &(*self.get_class_name()) {
                "text" => Ok(IupText(self.get_handle())),
                _ => Err(Some(self)),
            }
        }
    }

    fn as_timer(self) -> Result<IupTimer, Option<IupGenericWidget>> {
        unsafe {
            match &(*self.get_class_name()) {
                "timer" => Ok(IupTimer(self.get_handle())),
                _ => Err(Some(self)),
            }
        }
    }

    fn as_generic(self) -> Result<IupGenericWidget, Option<IupGenericWidget>> {
        Ok(self)
    }
}
impl RediscoveredCast for Option<IupGenericWidget> {
    fn as_dialog(self) -> Result<IupDialog, Option<IupGenericWidget>> {
        match self {
            Some(x) => x.as_dialog(),
            None => Err(None),
        }
    }
    fn as_label(self) -> Result<IupLabel, Option<IupGenericWidget>> {
        match self {
            Some(x) => x.as_label(),
            None => Err(None),
        }
    }
    fn as_hbox(self) -> Result<IupHbox, Option<IupGenericWidget>> {
        match self {
            Some(x) => x.as_hbox(),
            None => Err(None),
        }
    }
    fn as_vbox(self) -> Result<IupVbox, Option<IupGenericWidget>> {
        match self {
            Some(x) => x.as_vbox(),
            None => Err(None),
        }
    }
    fn as_bbox(self) -> Result<IupBbox, Option<IupGenericWidget>> {
        match self {
            Some(x) => x.as_bbox(),
            None => Err(None),
        }
    }
    fn as_text(self) -> Result<IupText, Option<IupGenericWidget>> {
        match self {
            Some(x) => x.as_text(),
            None => Err(None),
        }
    }
    fn as_timer(self) -> Result<IupTimer, Option<IupGenericWidget>> {
        match self {
            Some(x) => x.as_timer(),
            None => Err(None),
        }
    }
    fn as_generic(self) -> Result<IupGenericWidget, Option<IupGenericWidget>> {
        match self {
            Some(x) => Ok(x),
            None => Err(None),
        }
    }
}

#[derive(Debug)]
pub struct IupDialog(*mut Ihandle);
impl IupDialog {
    pub fn new() -> IupDialog {
        let ih;
        unsafe {
            ih = iup_sys::IupDialog(std::ptr::null_mut());
        }
        IupDialog(ih)
    }

    /// Removes decorations on the given dialog.
    ///
    /// Must be called after the dialog is mapped.
    #[cfg(windows)]
    pub fn win_remove_decorations(&mut self) {
        unsafe {
            let hwnd = self.get_raw_attr("HWND");
            if hwnd == std::ptr::null_mut() {
                return;
            }
            user32::SetWindowLongW(hwnd as winapi::HWND, winapi::GWL_STYLE, 0);
            user32::SetWindowLongW(hwnd as winapi::HWND, winapi::GWL_EXSTYLE, 0);
        }
    }

    /// Shows the object, and moves it to a specific position.
    pub fn move_xy(&mut self, x: XPos, y: YPos) {
        let x: u32 = x.into();
        let y: u32 = y.into();
        unsafe {
            IupShowXY(self.get_handle(), x as i32, y as i32);
        }
    }

    /// Offsets the object from its current location.
    pub fn offset_xy(&mut self, x_offset: i32, y_offset: i32) {
        let (mut x, mut y) = self.get_intint_attribute("SCREENPOSITION");
        x += x_offset;
        y += y_offset;
        unsafe {
            IupShowXY(self.get_handle(), x, y);
        }
    }
}
impl IupContainer for IupDialog {}
impl IupWidget for IupDialog {
    unsafe fn get_handle(&self) -> *mut Ihandle {
        self.0
    }
}

#[derive(Debug)]
pub struct IupLabel(*mut Ihandle);
impl IupLabel {
    pub fn new() -> IupLabel {
        let ih;
        unsafe {
            ih = iup_sys::IupLabel(std::ptr::null_mut());
        }
        IupLabel(ih)
    }

    pub fn set_label(&mut self, label: &str) {
        self.set_str_attribute("TITLE", label);
    }

    pub fn get_label(&self) -> Cow<str> {
        self.get_str_attribute("TITLE")
    }
}
impl IupWidget for IupLabel {
    unsafe fn get_handle(&self) -> *mut Ihandle {
        self.0
    }
}

#[derive(Debug)]
pub struct IupHbox(*mut Ihandle);
impl IupHbox {
    pub fn new() -> IupHbox {
        let ih;
        unsafe {
            ih = iup_sys::IupHbox(std::ptr::null_mut());
        }
        IupHbox(ih)
    }
}
impl IupContainer for IupHbox {}
impl IupWidget for IupHbox {
    unsafe fn get_handle(&self) -> *mut Ihandle {
        self.0
    }
}

#[derive(Debug)]
pub struct IupVbox(*mut Ihandle);
impl IupVbox {
    pub fn new() -> IupVbox {
        let ih;
        unsafe {
            ih = iup_sys::IupVbox(std::ptr::null_mut());
        }
        IupVbox(ih)
    }
}
impl IupContainer for IupVbox {}
impl IupWidget for IupVbox {
    unsafe fn get_handle(&self) -> *mut Ihandle {
        self.0
    }
}
#[derive(Debug)]
pub struct IupBbox(*mut Ihandle);
impl IupBbox {
    pub fn new() -> IupBbox {
        let ih;
        unsafe {
            ih = iup_sys::IupBackgroundBox(std::ptr::null_mut());
        }
        let mut bbox = IupBbox(ih);
        bbox.set_str_attribute("_ROXO_CLASS", "bbox");
        bbox
    }
}
impl IupContainer for IupBbox {}
impl IupWidget for IupBbox {
    unsafe fn get_handle(&self) -> *mut Ihandle {
        self.0
    }
}


#[derive(Debug)]
pub struct IupText(*mut Ihandle);
impl IupText {
    pub fn new() -> IupText {
        let ih;
        unsafe {
            ih = iup_sys::IupText(std::ptr::null_mut());
        }
        IupText(ih)
    }

    /// Get the text contents of the widget.
    pub fn get_contents(&self) -> Cow<str> {
        self.get_str_attribute("VALUE")
    }

    pub fn set_contents(&mut self, value: &str) {
        self.set_str_attribute("VALUE", value);
    }
}
impl IupWidget for IupText {
    unsafe fn get_handle(&self) -> *mut Ihandle {
        self.0
    }
}
#[derive(Debug)]
pub struct IupUser(*mut Ihandle);
impl IupUser {
    pub fn new() -> IupUser {
        let ih;
        unsafe {
            ih = iup_sys::IupUser();
        }
        IupUser(ih)
    }
}
impl IupWidget for IupUser {
    unsafe fn get_handle(&self) -> *mut Ihandle {
        self.0
    }
}
#[derive(Debug)]
pub struct IupTimer(*mut Ihandle);
impl IupTimer {
    pub fn new() -> IupTimer {
        let ih;
        unsafe {
            ih = iup_sys::IupTimer();
        }
        IupTimer(ih)
    }

    /// Activate the timer.
    pub fn start(&mut self) {
        self.set_bool_attribute("RUN", true);
    }

    /// Deactivate the timer.
    pub fn stop(&mut self) {
        self.set_bool_attribute("RUN", false);
    }

    /// Set the delay between timer calls in milliseconds.
    ///
    /// Limited by Iup implementation details; refer to the Iup docs.
    pub fn set_delay(&mut self, delay: i32) {
        self.set_int_attribute("TIME", delay);
    }
}
impl IupWidget for IupTimer {
    unsafe fn get_handle(&self) -> *mut Ihandle {
        self.0
    }
}

// Since this can leak memory if misused, I'm calling it unsafe.
unsafe fn to_ffi(s: &str) -> *mut std::os::raw::c_char {
    // NOTE: Technically less efficient than we can be, since CString will
    // allocate and also do a check for null bytes.
    std::ffi::CString::new(s).unwrap().into_raw()
}

unsafe fn free_ffi(p: *mut std::os::raw::c_char) {
    std::ffi::CString::from_raw(p);
}

pub fn set_global_attribute(attribute: &str, value: &str) {
    unsafe {
        let iupattr = to_ffi(attribute);
        let iupval = to_ffi(value);
        IupSetStrAttribute(std::ptr::null_mut(), iupattr, iupval);
        // IUP internally duplicates these strings.
        free_ffi(iupattr);
        free_ffi(iupval);
    }
}
